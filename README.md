# client-server-monitoring
This application consist of two parts:
- AlphaClient: A utility that can report the metrices of number of ssh log-in attempts (on the machine it is running) to AlphaServer. As soon as the ssh log-in attempt happens it should get
- AlphaServer: A simple service (simple web-app or data query client) that can show all the metrices sent to it by various AlphaClients (asdescribed later).

## What to improve if the application get bigger
- Put the load balancer in front of AlphaServer
- Deploy the AlphaServer using autoscaling group/instance group
- Use RDMS like MySQL or PostgreSQL to store metrics and support the master-replica
- Using a Messaging Queue system to temporary store metrics sent from AlphaClient to reduce database load.
  - a worker to pull metrics from Queue and push to database.

## Requirements
- python3
- sqlite

## How to run the scripts manually:

### Alpha Client
Run these commands on a client machine, the `alphaclient.py` will monitor the SSH log file /var/log/auth.log and send the metrics for any failed login.
```
$ pip install requests
$ export ALPHA_SERVER_IP=X.X.X.X
$ python3 alpha-client/alphaclient.py
```

### Alpha Server
Run these commands on a server machine and the application will listen to port 5000.

```
$ python3 -m venv virtualenv; source virtualenv/bin/activate
$ pip install flask flask-sqlalchemy
$ cd alpha-server
$ python app.py
```

Try to log into your client machine with the wrong password and then open the url `http://server-ip:5000` (change the server-ip with your server ip address)


## How to run the scripts and deploy automatically using Ansible
Edit the `hosts` file in the `deployment` folder with the IP address of your `server` & `client`

### For alphaserver application

```
$ cd deployment
$ ansible-playbook deploy.yml -i hosts -e target_host=server
```

### For alphaclient application

```
$ cd deployment
$ ansible-playbook deploy.yml -i hosts -e target_host=client -e alpha_server_ip=x.x.x.x
```
