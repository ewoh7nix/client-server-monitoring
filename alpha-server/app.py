from flask import Flask
from flask import request
from flask import render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///monitoring.sqlite3'

db = SQLAlchemy(app)

class SSH(db.Model):
  __tablename__ = 'ssh'

  _id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  ip = db.Column(db.String(30), nullable=False)
  message = db.Column(db.String(100), nullable=False)

  def __init__(self, ip, message):
    self.ip = ip
    self.message = message

@app.route("/")
def home():
  """
  show dashboard that will query to database and show the data.
  """
  ssh_data = []
  data = db.session.execute('select ip, count(*) from ssh group by ip')
  for d in data:
    ssh_data.append(d)
  return render_template('home.html', ssh_data=ssh_data)

@app.route("/metrics", methods=["POST"])
def metrics():
  """
  POST json data and store it into database
  """
  data = request.get_json()
  ip = data['ip']
  message = data['message']

  ssh_data = SSH(ip, message)
  db.session.add(ssh_data)
  db.session.commit()
  return {ip: message}


if __name__ == '__main__':
  db.create_all()
  app.run(debug=True)