import time
import os
import re
import requests
import socket

ALPHA_SERVER_IP = os.getenv('ALPHA_SERVER_IP')
URI = 'http://{}:5000/metrics'.format(ALPHA_SERVER_IP)
LOGFILE = '/var/log/auth.log'

def tail(file):
  file.seek(0,2)
  while True:
    line = file.readline()
    if not line:
      time.sleep(1)
      continue
    yield line

def send_metrics(data):
  r = requests.post(URI, json=data)
  if r.status_code != 200:
    print('Error: failed to connect to alphaserver')

def get_local_ip():
  """
  Get a local IP address that connected to the internet
  """
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.connect(("1.1.1.1", 53))
  return s.getsockname()[0]

if __name__ == '__main__':
  logfile = open(LOGFILE, 'r')
  lines = tail(logfile)
  for line in lines:
    if not re.search('sshd.*Failed', line):
      continue
    data = {'ip': get_local_ip(), 'message': line.strip()}
    print(data)
    send_metrics(data)